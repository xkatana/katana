# katana

katana is a mobile application for wechat conference [katana wechat conference](https://bitbuckt.org/nonumber1989/katana)

## about the architecture
> katana is build with [angularjs](https://angularjs.org/) and [ionic](http://ionicframework.com/)

> angularjs provide MVC javascript framework and ionic provide a good material design css 

> katana need wechat platform support 
## prerequisite
    nodejs
    npm install -g gulp
    npm install -g bower
    git bash

## katana configuration
    git clone katana
    cd katana
    npm install
    bower install

## Build & development
    gulp serve