'use strict';

var katana = angular.module('katana');
katana.factory('AccountService', ['$localStorage', function($localStorage) {

	var setAccount = function(account) {
		$localStorage.user = account;
	}

	var setUserToken = function(token) {
		$localStorage.userToken = token;
	}

	var getAccount = function() {
		var account = $localStorage.user;
		return account;
	}

	return {
		getAccount: getAccount,
		setAccount: setAccount
	};
}]);