'use strict';

var katana = angular.module('katana');
katana.factory('KatanaInterceptor', ['$rootScope', '$injector', function($rootScope, $injector) {

  var showDialog = function(theScope) {
    $injector.invoke(function($ionicModal, $state) {
      $ionicModal.fromTemplateUrl('views/katana/interceptorDialog.html', {
        scope: theScope,
        animation: 'slide-in-up'
      }).then(function(modal) {
        theScope.modal = modal;
        theScope.modal.show();
        theScope.continue = function() {
          theScope.modal.hide();
          console.log($state.current.name);
          $state.go('katana.conference.schedule');
        };

      });
    });
  };
  return {
    request: function(config) {
      $rootScope.$broadcast('loading:show');
      return config;
    },
    response: function(response) {
      $rootScope.$broadcast('loading:hide');
      return response;
    },
    responseError: function(rejection) {
      $rootScope.$broadcast('loading:hide');
      console.log(JSON.stringify(rejection));
      var rejectionError = {};
      if (rejection.data === null) {
        $rootScope.networkRejecttion = {
          message: "network error !"
        };
      } else if (rejection.status === 401) {
        $rootScope.networkRejecttion = {
          message: "network error !"
        };
        // $injector.invoke(function($state) {
        //   console.log($state.current.name);
        //   $state.go('tachi.account.signIn');
        // });
      } else if (rejection.status === 403) {

      } else if (rejection.status === 404) {

      } else if (rejection.status === 500) {

      } else {

      }
      showDialog($rootScope);
    }
  };
}]);