'use strict';

var katana = angular.module('katana');
katana.controller('ExamController', ['$scope', 'Exam', function($scope, Exam) {
	Exam.query(function(result) {
		$scope.exams = result;
	});

}]);