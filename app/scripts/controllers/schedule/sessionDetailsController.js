'use strict';

var katana = angular.module('katana');
katana.controller('SessionDetailsController', function($scope, $stateParams,$ionicModal) {
    $scope.id = $stateParams.id;
    $scope.session = {
        "id": 1,
        "name": "Introduction to HANA CLOUD PLATFORM",
        "location": "Room 2203",
        "description": "SAP HANA is an in-memory, column-oriented, relational database management system developed and marketed by SAP SE.[2][3] Its primary function as database server is to store and retrieve data as requested by the applications. In addition, it performs advanced analytics (predictive analytics, spatial data processing, text analytics, text search, streaming analytics, graph data processing) and includes ETL capabilities and an application server.",
        "speakerNames": ["Brandy Carney"],
        "timeStart": "9:00 am",
        "timeEnd": "9:30 am",
        "tracks": ["Ionic"]
    };

 $ionicModal.fromTemplateUrl('views/katana/questionDialog.html', {
    scope: $scope,
    animation: 'slide-in-up'
  }).then(function(modal) {
    $scope.modal = modal;
  });
  $scope.openModal = function() {
    $scope.modal.show();
  };
  $scope.closeModal = function() {
    $scope.modal.hide();
  };
  //Cleanup the modal when we're done with it!
  $scope.$on('$destroy', function() {
    $scope.modal.remove();
  });
  // Execute action on hide modal
  $scope.$on('modal.hidden', function() {
    // Execute action
  });
  // Execute action on remove modal
  $scope.$on('modal.removed', function() {
    // Execute action
  });
});
