'use strict';

var katana = angular.module('katana');

katana.controller('KatanaController', function($scope, $ionicPopover, $translate, $state, AccountService) {


  // $scope.user = AccountService.getAccount();
   $scope.user = {
    "name":"Steven Xu",
    "image":"images/yeoman.png",
    "briefIntro":"SAP engineer,java node js ..."
   };

  //for seetings popover use
  $ionicPopover.fromTemplateUrl('views/katana/settingsPopover.html', {
    scope: $scope
  }).then(function(popover) {
    $scope.popover = popover;
  });

  $scope.openPopover = function($event) {
    $scope.popover.show($event);
  };
  $scope.closePopover = function() {
    $scope.popover.hide();
  };
  //Cleanup the popover when we're done with it!
  $scope.$on('$destroy', function() {
    $scope.popover.remove();
  });
  // Execute action on hide popover
  $scope.$on('popover.hidden', function() {
    // Execute action
  });
  // Execute action on remove popover
  $scope.$on('popover.removed', function() {
    // Execute action
  });

});