'use strict';

var katana = angular.module('katana');
katana.controller('SpeakerDetailsController', function($scope,$stateParams,Speakers) {
	$scope.speakers = [];
    Speakers.query(function(speakers) {
        $scope.speakers = speakers;
    });
})