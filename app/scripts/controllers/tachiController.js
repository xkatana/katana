'use strict';

var katana = angular.module('katana');

katana.controller('TachiController', function($scope, $state,$ionicSlideBoxDelegate, Configuration) {
	//when skip the information page,start the katana application
	$scope.startKatana = function() {
		$state.go('tachi.account.signUp');
	};

    $scope.slides = [];
	$scope.configuration = Configuration.get();
	$scope.configuration.$promise.then(function(result) {
		$scope.slides = result.slides;
		$ionicSlideBoxDelegate.$getByHandle('image-viewer').update();
	});

});