'use strict';

var katana = angular.module('katana');
katana.controller('AccountController', function($scope, $state, $translate, Account, AccountService) {
    //account is used for sign in and sign up
    $scope.account = {};
    $scope.signIn = function() {
        Account.save($scope.account, function(result) {
            AccountService.setAccount(account);
            $state.go('katana.conference.schedule');
        }, function(err) {
            console.log(err)
        });
    };
    $scope.signUp = function() {
        Account.save($scope.account, function(result) {
            AccountService.setAccount(account);
            $state.go('katana.conference.schedule');
        });
    };

    //user information is used for account information maintanaince
    $scope.userInformation = {
        "userName": "Steven.Xu",
        "weChat": "nonumber1989",
        "accountName": "徐玉强",
        "address": "ShangHai Pudong district",
        "sex": "male",
        "signature": "I am a SAP engineer joined since Nov. 2015",
        "preferLanguage": "zh"
    };
    $scope.settingsList = [{
        text: "Enable Record Provite Information ",
        checked: true
    }];

    // Language.query(function(result) {
    //     $scope.languages = result;
    // }, function(error) {

    // });
    // $scope.getLanguageText = function(value) {
    //     return _.result(_.find($scope.languages, function(theOne) {
    //         return theOne.value === value;
    //     }), 'text');
    // };
    $scope.swithLanguage = function() {
        $translate.use($scope.userInformation.preferLanguage);
    };
});